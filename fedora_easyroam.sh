#!/bin/bash


# Helper functions
initialize_files () {
    touch "$easyroam_path/easyroam_client_cert.pem"
    touch "$easyroam_path/easyroam_client_key.pem"
    touch "$easyroam_path/easyroam_root_ca.pem"
    chmod -R 700 "$easyroam_path"
}

get_id() {
    grep th-ab.de easyroam_client_cert.pem | \
        cut -d ',' -f1 | \
        cut -d"=" -f3 | \
        tr -d " "
}

# CLI Argurments
easyroam_p12_cert="$1"
secret_password="$2"

# Configuration
easyroam_path=~/.local/opt/easyroam

# Preparing target directory
mkdir -p "$easyroam_path"
cp -u "$easyroam_p12_cert" "$easyroam_path/"
initialize_files
cd "$easyroam_path"


# Creating certs from pkcs file
openssl pkcs12 -in "$easyroam_p12_cert" -passin "pass:" -legacy -nokeys \
    > easyroam_client_cert.pem

openssl pkcs12 -legacy -in "$easyroam_p12_cert" -passin "pass:" -nodes -nocerts -legacy | \
    openssl rsa -legacy -aes256 -passout "pass:$secret_password" -out easyroam_client_key.pem

openssl pkcs12 -in $easyroam_p12_cert -cacerts -passout "pass:$secret_password" -passin "pass:" -legacy \
    > easyroam_root_ca.pem

# Output of identity needed for nm-connection-editor
echo -e "Your Identity is: \n$(get_id)"
