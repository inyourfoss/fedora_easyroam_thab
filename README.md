# Usage
## Cert generation
### CLI Version
In `fedora_easyroam.sh` change `easyroam_path` to the desired location.
```
# Configuration
easyroam_path=~/.local/opt/easyroam
```

Then run
```
./fedora_easyroam.sh "my_easyroam_cert.p12" "MySu9erS3cretP4ssW0r6"
```
- The first argument is the path to the pkcs12 file.
- The second argument is the password you set for the encryption. 
- You will need it later again with `nm-connection-editor`.


### GUI Version
To start run
```
./fedora_easyroam_gui.sh
```
1. You will be prompted to select your pkcs12 file you got from EasyRoam.
2. You will be prompted to select the target directory. I suggest `~/.config/easyroam/`. Create it if needed.
3. You will be prompted to enter an **encryption password** twice for verification **(Keep for later)**. 
4. Your **Identity** needed for `nm-connection-editor` will be shown **(Keep for later)**. You can copy and paste it where needed.

## Configuration
Run `nm-connection-editor` in your terminal.
Further instructions will follow.
