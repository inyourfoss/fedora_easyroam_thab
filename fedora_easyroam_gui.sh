#!/bin/bash

# Helper functions
verify_password() 
{
    [ "$1" != "$(zenity --entry --text='Bitte Passwort nochmal eingeben' --hide-text)" ] && echo "Wrong password. Exiting..." && exit
    zenity --info --text="Password wurde Verifiziert" 
}

get_id() 
{
    grep th-ab.de easyroam_client_cert.pem | \
        cut -d ',' -f1 | \
        cut -d"=" -f3 | \
        tr -d " "
}

initialize_files() 
{
    touch "$easyroam_path/easyroam_client_cert.pem"
    touch "$easyroam_path/easyroam_client_key.pem"
    touch "$easyroam_path/easyroam_root_ca.pem"
    chmod -R 700 "$easyroam_path"
}

# User input via zenity
easyroam_p12_cert="$(zenity --file-selection --title='PKCS12 Datei auswählen')"
[ ! -z "$easyroam_p12_cert" ] && easyroam_path="$(zenity --file-selection --directory --title='Zielornder auswählen')" || exit
[ ! -z "$easyroam_path" ] && secret_password="$(zenity --password)" || exit
[ ! -z "$secret_password" ] && verify_password "$secret_password"

# Preparing target directory
mkdir -p "$easyroam_path"
cp -u "$easyroam_p12_cert" "$easyroam_path/"
initialize_files
cd "$easyroam_path"


# Creating certs from pkcs file
openssl pkcs12 -in "$easyroam_p12_cert" -passin "pass:" -legacy -nokeys > easyroam_client_cert.pem

openssl pkcs12 -legacy -in "$easyroam_p12_cert" -passin "pass:" -nodes -nocerts -legacy | \
    openssl rsa -legacy -aes256 -passout "pass:$secret_password" -out easyroam_client_key.pem

openssl pkcs12 -in $easyroam_p12_cert -cacerts -passout "pass:$secret_password" -passin "pass:" -legacy > easyroam_root_ca.pem


# Output of identity needed for nm-connection-editor
zenity --entry --width 450 --entry-text="$(get_id)" --text="Your Identity is: (Copy and then paste into nm-connection-editor)"
